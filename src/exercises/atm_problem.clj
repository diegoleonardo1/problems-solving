(ns exercises.atm-problem)

(def available_bank_notes [1 5 10 25 50 100 200])
(def available_bank_notes_with_quantity {200 [1]
                                         100 [1]
                                         50  [100]
                                         25  [25]
                                         10  [100]
                                         5   [40]
                                         1   [10]})

(defn withdraw [value]
  (if (<= value 0)
    []
    (let [available_cash (sort-by - available_bank_notes)]
      (loop [remaining-value value
             banknotes []]
        (if (= remaining-value 0)
          banknotes
          (let [v (->> available_cash
                       (filter #(>= remaining-value %))
                       first)]
            (recur (- remaining-value v)
                   (conj banknotes v))))))))

(defn withdraw-v2 [value]
  (if (<= value 0)
    []
    (loop [remaining-value value
           bank-notes      available_bank_notes_with_quantity
           result          []]
      (if (= remaining-value 0)
        result
        (let [[k _] (->> bank-notes
                         (filter (fn [[k quantity]]
                                   (let [qtd (first quantity)]
                                     (and (>= remaining-value k)
                                          (>= qtd 1)))))
                         first)
              new-state (update bank-notes k #(-> (first %)
                                                  dec
                                                  vector))]
          (recur (- remaining-value k)
                 new-state
                 (conj result k)))))))

(comment

  (withdraw-v2 479)

  (reduce + (withdraw 600))

  (count (withdraw 600))

  (count (withdraw 70))

  (withdraw 400)

  (withdraw 75)

  (withdraw 76)

  (withdraw 6)

  (withdraw 4)

  (reduce + (withdraw-v2 600))

  (count (withdraw-v2 600))

  (count (withdraw-v2 70))

  (withdraw-v2 400)

  (withdraw-v2 75)

  (withdraw-v2 76)

  (withdraw-v2 6))
