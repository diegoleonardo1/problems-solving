(ns exercises.count-words)

(def example-input "\"That's the password: 'PASSWORD 123'!\",  cried the Special Agent.\nSo I fled.")

(defn count-words [input]
  (-> (clojure.string/split input #" ")
      (frequencies)))
