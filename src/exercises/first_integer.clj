(ns exercises.first-integer)

(def collection [-3 -1 1 2 3 4 6])

(defn- get-first-positive-number
  ([pos-coll] (get-first-positive-number pos-coll nil))
  ([pos-coll positive-number]
   (if (= nil positive-number)
     (let [seq-number (+ (first pos-coll) 1)
           rest (rest pos-coll)]
       (if (or (= seq-number (second pos-coll))
               (some #(= seq-number %) pos-coll))
         (get-first-positive-number rest positive-number)
         seq-number))
     positive-number)))

(defn first-positive-number [collection]
  (let [coll (filter pos? collection)]
    (if (empty? coll)
      1
      (-> coll
          sort
          get-first-positive-number))))

#_(first-positive-number [])
#_(first-positive-number nil)
#_(first-positive-number [1 2 3 5 6 7 8])
#_(first-positive-number collection)

(defn- get-first-positive-number-loop [coll]
  (let [limit (count coll)]
    (loop [index 0
           result nil]
      (if (or (= index limit)
              (not (nil? result)))
        result
        (recur (inc index)
               (let [seq-number (inc (nth coll index))]
                 (if (or (= (inc index) limit)
                         (= seq-number (nth coll (inc index) nil))
                         (some #(= seq-number %) coll))
                   nil
                   seq-number)))))))

(defn first-positive-number-loop [collection]
  (let [coll (filter pos? collection)]
    (if (empty? coll)
      1
      (-> coll
          sort
          get-first-positive-number-loop))))

#_(first-positive-number-loop [1 2 3 5 6 7 8])
#_(first-positive-number-loop collection)

(defn- proccess-in-chunks [collection]
  (let [coll (partition 10 collection)
        limit (count coll)]
    (loop [index 0
           result nil]
      (if (or (= index limit)
              (not (nil? result)))
        result
        (recur (inc index)
               (get-first-positive-number-loop (nth coll index)))))))

(defn first-positive-number-in-chunks [collection]
  (let [coll (filter pos? collection)]
    (if (empty? coll)
      1
      (-> coll
          sort
          proccess-in-chunks))))

#_(let [n (range 20001)
        nr (remove #(= 9870 %) n)]
    (time (first-positive-number-in-chunks nr)))

(defn proccess-positive-number [coll]
  (let [limit (count coll)]
    (cond
      (>= limit 5000) (first-positive-number-in-chunks coll)
      :else (first-positive-number-loop coll))))

#_(let [n (range 6000)
        nr (remove #(= 5500 %) n)]
    (time (first-positive-number-loop nr)))

#_(let [n (range 20001)
        nr (remove #(= 9870 %) n)]
    (time (first-positive-number-in-chunks nr)))

#_(let [n (range 1000002)
      nr (remove #(= 980000 %) n)]
  (time (proccess-positive-number nr)))

#_(time (proccess-positive-number collection))
#_(time (proccess-positive-number [1 2 3 4 5 6 7 8 10 11]))
