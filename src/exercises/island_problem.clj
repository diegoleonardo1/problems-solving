(ns exercises.island-problem)

;; Dada uma matriz m x n de zeros e uns, escreva uma função que retorne o número de ilhas nessa matriz.
;; As ilhas são definidas por um ou um conjunto de números 1 conectados verticalmente ou horizontalmente.

;; Given a matrix m x n of zeros and ones, write a function that returns the number of the islands on that.
;; The islands are defined by a set of the number one connected vertically or horizontally.

;;input exemplo:
(def input [[1 0 1 0]
            [0 1 1 0]
            [1 0 1 0]])

;; [0,0] -> é uma ilha (It's an island)
;; [0,2] -> é uma ilha (It's an island)

;; Resposta do input exemplo: 3 ilhas
;; Expected response for the example input: 3 islands

(defn was-visited? [visited row index]
  (true? (some (fn [[r c]]
                 (and (= r row)
                      (= c index)))
               visited)))

(defn visit? [input visited [row index]]
  (let [count-input (count input)
        limit-input (count (first input))
        visit (and (and (>= row 0)
                      (<= row count-input)
                      (>= index 0)
                      (<= index limit-input))
                 (false? (was-visited? visited row index)))]
    (true? visit)))

(defn insert [col row index]
  (if (was-visited? col row index)
    col
    (into col [[row index]])))

(defn position-visited? [visited row index]
  (true? (some (fn [elem]
                 (some (fn [item]
                         (let [[row-in index-in] item]
                           (and (= row-in row)
                                (= index-in index))))
                       elem))
               visited)))

(defn explore
  ([input row index visited] (explore input row index visited [] []))
  ([input row index visited result root]
   (let [current     (get-in input [row index] 0)
         new-visited (if (was-visited? visited row index)
                       visited
                       (into visited [[row index]]))]
     (if (= current 0)
       (let [new-root              (if (empty? root)
                                     [[0 0]]
                                     root)
             [first-row first-col] (peek new-root)]
         (recur input first-row first-col new-visited result (pop new-root)))
       (let [new-result (insert result row index)
             visit      (partial visit? input new-visited)]
         (cond
           (visit [(inc row) index]) (do #_(println "south")
                                         (let [new-root (into root [[row index]])]
                                           #_(println "new-root" new-root)
                                           (recur input (inc row) index new-visited new-result new-root)))

           (visit [(dec row) index]) (do #_(println "north")
                                         (let [new-root (into root [[row index]])]
                                           #_(println "new-root" new-root)
                                           (recur input (dec row) index new-visited new-result new-root)))

           (visit [row (inc index)]) (do #_(println "east")
                                         (let [new-root (into root [[row index]])]
                                           #_(println "new-root" new-root)
                                           (recur input row (inc index) new-visited new-result new-root)))

           (visit [row (dec index)]) (do #_(println "west")
                                         (let [new-root (into root [[row index]])]
                                           #_(println "new-root" new-root)
                                           (recur input row (dec index) new-visited new-result new-root)))

           (> (count root) 0) (do #_(println "path")
                                  (let [[row index] (peek root)]
                                    #_(println "root" root "path row" row "path index" index)
                                    (recur input row index new-visited new-result (pop root))))

           :else new-result))))))

#_ (explore [[1 1 1 1]
             [1 0 0 1]
             [0 1 0 0]]
            0 0
            [])

#_(explore input 0 0 [])

(defn- explored-islands [input row visited]
  (let [limit (-> input
                  first
                  count)]
    (loop [index  0
           result visited]
      (if (= index limit)
        result
        (recur (inc index)
               (if (or (position-visited? result row index)
                       (= (get-in input [row index] 0) 0))
                 result
                 (let [new-result (explore input row index result)]
                   (if (empty? new-result)
                     result
                     (conj result new-result)))))))))

(defn- islands [input]
  (let [limit (count input)]
    (loop [row    0
           result []]
      (if (= row limit)
        result
        (recur (inc row)
               (explored-islands input row result))))))

(defn number-of-islands [input]
  (-> input
      islands
      count))

(def example [[1 0 1 0]
              [0 1 1 0]
              [1 0 1 0]])

#_(number-of-islands example)
