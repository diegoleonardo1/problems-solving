(ns exercises.math-operations-with-string)

;; Basically the problem is given the input string "1 plus 2", the function should return 3

(def operations
  {"times" *
   "plus"  +
   "negative" -})

(defn number? [v]
  (false? (nil? (re-matches (re-pattern "[0-9]|-[0-9]") v))))

(defn eval-string [exp]
  (let [elem    (clojure.string/split exp #" ")
        numbers (->> elem
                     (filter #(number? %))
                     (map #(Integer/parseInt %)))]
    (reduce (get operations (second elem)) numbers)))

(eval-string "1 plus 2")
(eval-string "4 plus 4")

#_(eval-string "negative 1 plus 2")

(defn str->op [v]
  (-> v
      (clojure.string/replace #"times" "*")
      (clojure.string/replace #"plus" "+")
      (clojure.string/replace #"negative" "-")))

(let [ n (-> (str->op "1 plus 2 times 4 times 4")
             (clojure.string/split #" "))]
  (partition 2  2 n))

(eval (read-string "(+ 3 (+ 1 (+ 1)))"))

"2 times 2 times 2 times 2"
(* 2 2 2 2)

"3 plus 2 times 2"
(+ 3 (* 2 2))

"negative 3 plus 2 times 2"
(+ (- 3) (* 2 2))

"negative 3 plus 2"
(+ (- 3) 2)


(reduce + [-3 2])

(subvec (clojure.string/split "1 plus 2 plus 3" #" ") 0 3)

(re-seq (re-pattern "[0-9] (plus|negative|times) [0-9]") "1 times 2 times 2")

(re-find (re-pattern "[0-9]| ^negative [0-9]") "3 plus 2 negative 2")
