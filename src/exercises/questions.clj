(ns exercises.questions
  (:gen-class))

(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))

(defn split-coll [x coll]
  (loop [n          1
         result     []
         rest-input coll]
    (if (> n x)
      result
      (recur (inc n)
             (conj result (take x rest-input))
             (rest rest-input)))))

(defn part-coll [x coll]
  (->> (partition x 1 coll)
       (map (fn [x]
              (apply min x)))
       (apply max)))

(defn binary-search [coll element]
  (loop [left  0
         right (dec (count coll))]
    (if (<= left right)
      (let [m      (int (Math/floor (/ (+ left right) 2)))
            actual (nth coll m)]
        (println "left" left "right" right "m" m "actual" actual)
        (cond
          (= actual element) true
          (< actual element) (recur (+ m 1) right)
          (> actual element) (recur left (- m 1))))
      false)))

(defn pivot [coll inicio fim]
  (let [pivot (nth coll fim)
        i inicio]
    (doseq [j (range inicio fim)]
      (if (<= (nth coll j) pivot)
        (-> (update coll j #(nth coll i))
            (update i #(nth coll j)))))))

(defn quick-sort [[pivot & coll]]
  (when pivot
    (concat (quick-sort (filter #(< % pivot) coll))
            [pivot]
            (quick-sort (filter #(> % pivot) coll)))))

(concat [1] [2] [3] [4])
(merge [1] [2] [3] [4])
(merge [1] 2 3 4)


(defn merge-sort
  "sorting the given collection with merge-sort"
  [coll]
  (if (or (empty? coll) (= 1 (count coll)))
    coll
    (let [[l1 l2] (split-at (/
                             (count coll)
                             2)
                            coll)]
                                        ;recursive call
      (loop [r []
             l1 (merge-sort l1)
             l2 (merge-sort l2)]
                                        ;merging
        (cond (empty? l1) (into r l2) ;when l1 is exhausted
              (empty? l2) (into r l1) ;when l2 is exhausted
              :else (if (> 0 (compare (first l1) (first l2))) ;comparison
                      (recur (conj r (first l1)) (rest l1) l2)
                      (recur (conj r (first l2)) l1 (rest l2))))))))

(->> (partition 2 2 {"about" "bar"})
     (reduce (fn [c [k v]]
               (assoc c (keyword k) v))
             {}))

(comment

  (subvec [1 2 3 4 5 6 7 8] 4 8)

  (binary-search (range 1000000) 999000)

  (quick-sort [1])

  (quick-sort [1 0 -1])

  (quick-sort '(2 1 4 3))

  (time (quick-sort [2 4 1 9 6 7 5 8 3]))

  (time (merge-sort [4 7 2 6 4 1 8 3 5]))

  (def input [2 5 4 6 8])
  (def x1 3)

  (def input-2 [1 2 3 1 2])
  (def x2 1)

  (def input-3 (range 1000))
  (def x3 1000)

  (split-coll x2 input-2)

  )
