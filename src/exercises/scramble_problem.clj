(ns exercises.scramble-problem
  (:require [clojure.string :as cstr]
            [malli.core :as m]
            [malli.error :as me]
            [malli.generator :as mg]
            [malli.util :as mu]
            [malli.instrument :as mi]))

(defn- remove-str [s value]
  (let [index-v (cstr/index-of s value)
        new-s   (cstr/split s #"")]
    (-> (conj (subvec new-s 0 index-v) (subvec new-s (inc index-v) (count s)))
        flatten
        cstr/join)))

(defn- match-word? [expr w]
  (let [current-letter (-> w
                           first
                           str)
        result         (cstr/includes? expr current-letter)]
    (if (or (false? result)
            (empty? w))
      result
      (recur (remove-str expr current-letter)
             (rest w)))))

(defn scramble? [expr w]
  (if (< (count expr) (count w))
    false
    (match-word? expr w)))

(def scramble (m/schema [:and
                         [:string {:min 1}]
                         [:re {:error/message "Should only contain lowercase letters"} #"^[a-z]+$"]]))

(def scble?
  (m/-instrument {:schema [:=>
                           [:cat scramble scramble]
                           [:boolean]]}
                 (fn [expr v]
                   (scramble? expr v))))

(-> (scble? "odgo" "good")
    (me/humanize))

(defn scrb?
  "scramble validation inputs"
  {:malli/schema [:=> [:cat scramble scramble] [:boolean]]}
  [expr v]
  (scramble? expr v))

#_(mi/collect!)
(mi/instrument!)

(comment

  (scramble? "godfo" "good")

  (scramble? "god" "good")

  (scramble? "rekqodlw" "world")
  (scramble? "cedewaraaossoqqyt" "codewars")
  (scramble? "katas"  "steak")

  (defn- match-word-v1
    ([w expr] (match-word-v1 w expr nil))
    ([w expr result]
     (if (or (empty? w)
             (false? result))
       result
       (let [current (-> w
                         first
                         str)
             includes? (cstr/includes? expr current)
             new-expr (if includes?
                        (remove-str expr current)
                        expr)]
         (recur (rest w)
                new-expr
                includes?)))))

  (match-word? "godfoo" "good")

  (defn match-word-loop? [w e]
    (let [limit (count w)
          word (clojure.string/upper-case w)
          word-set (clojure.string/upper-case e)]
      (loop [index  0
             result nil]
        (if (or (= index limit)
                (false? result))
          result
          (recur (inc index)
                 (true? (clojure.string/includes? word-set (str (nth word index)))))))))

  (def w "Pneumonoultramicroscopicsilicovolcanoconiosis")
  (def rw "adghuboxlkjsdfewqrtyuiogfdhdjsazxcvnbmp")

  (match-word? w rw)

  (defn string->set [s]
    (-> s
        (clojure.string/lower-case)
        (clojure.string/split #"")
        set))

  (defn match-word? [w c]
    (let [str1 (string->set w)
          str2 (string->set c)]
      (clojure.set/subset? str1 str2)))

  (do (print "loop ")
      (time (match-word-loop? w rw)))

  (do (print "loop ")
      (time (match-word-loop? "world" "rekqodflw")))

  (do (print "subset ")
      (time (match-word? "world" "rekqodflw")))

  (do (print "subset ")
      (time (match-word? w rw)))

  (do (print "match-word? ")
      (time (match-word? "world" "rekqodflw")))

  (match-word-loop? "good" "god")

  (m/validate scramble "n")
  (m/validate scramble "foobar")
  (m/validate scramble "+f")
  (m/validate scramble "f+")
  (m/validate scramble "\n")
  (m/validate scramble "i\n")
  (m/validate scramble "@a")
  (m/explain scramble "")

  (-> (m/explain scramble "")
      (me/humanize))

  (-> (m/explain scramble "aA")
      (me/humanize))

  (mg/sample scramble)
  (mg/generate scramble)

  (scrb? "odog" "good")

  (-> (scrb? "odgoG" "good")
      (me/humanize))

  (-> (m/explain scramble "")
      (me/humanize))
)
