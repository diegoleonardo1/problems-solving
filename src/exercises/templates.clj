(ns exercises.templates
  (:require [clojure.walk :as cw]))

(def template "Hi my name is {#name}. I am living in {#country}")

(def data-test [{:name "Alex Turner"}
                {:country "England"}])

(defn clear-unmatched [clear? s]
  (if clear?
    (clojure.string/replace s
                            (re-pattern "\\{#[a-z]+\\}")
                            "")
    s))

(defn filled-template [template data clear-unmatched?]
  (->> (cw/stringify-keys data)
       (reduce (fn [acc elem]
                 (let [key (-> elem
                               keys
                               first)
                       val (get elem key)
                       regex (re-pattern (str "\\{#" key "\\}"))]
                   (clojure.string/replace acc regex val)))
               template)
       (clear-unmatched clear-unmatched?)))

(comment
  (def unmatched-template "Hi my name is {#name} and today is {#date}. I am living in {#country}")


  #_(filled-template template data-test true)
  #_(filled-template unmatched-template data-test true)
  #_(filled-template unmatched-template data-test false)

  #_(filled-template template
                     {"name" "Tom Morello"
                      "country" "Mexico"}
                     false)

  #_(clojure.string/includes? "Oi {#Name}" "{#Date}")

  #_(clojure.string/replace "and today is {#date}"
                            (re-pattern "\\{#[a-z]+\\}")
                            "")

  (reduce (fn [acc [key val]])
          ""
          [{:name "diego"}])


  (def f (future (Thread/sleep 10000) (println "done") 100))

  @f


  (let [a (future (Thread/sleep 10000) (println "feito") 10)]
    @a
    "foobar")


  (let [p (promise)]
    (let [angieslist "https://www.google.com"]
      (doseq [url [angieslist]]
        (future (let [response (slurp url)]
                  (deliver p response)))))
    @p)

  (slurp "https://www.google.com")
  (slurp "https://www.espn.com.br")

  (def d "foo")




  (defn foo [coll]
    (map str coll))

  (foo [1 2 3 4])

  (def d (foo [1 2 3 4]))

  (def d (doall (map println [1 2 3 4])))

)
